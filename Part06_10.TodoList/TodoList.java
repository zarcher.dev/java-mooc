import java.util.ArrayList;


public class TodoList {
    
    private ArrayList<String> items;
    
    public TodoList(){
        this.items = new ArrayList<>();
    }
    
    public void add(String task){
        this.items.add(task);
    }
    
    public void print(){
        int index = 1;
        for (String item: items){
            System.out.println(index + ": " + item);
            index++;
        }
    }
    
    public void remove(int number){
        items.remove(number - 1);
    }
}
