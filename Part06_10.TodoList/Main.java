import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        TodoList list = new TodoList();
        Scanner scan = new Scanner(System.in);
        UserInterface uI = new UserInterface(list, scan);
        uI.start();

    }
}
