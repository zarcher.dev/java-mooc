
import java.util.ArrayList;

public class Hold {

    private int maxWeight;
    private int currentWeight;
    private ArrayList<Suitcase> suitcases = new ArrayList<>();

    public Hold(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public void addSuitcase(Suitcase suitcase) {
        if (suitcase.totalWeight() + this.currentWeight <= this.maxWeight) {
            this.suitcases.add(suitcase);
            this.currentWeight += suitcase.totalWeight();
        }
    }

    public String toString() {
        return this.suitcases.size() + " suitcases (" + this.currentWeight + " kg)";
    }

    public void printItems() {
        for (Suitcase singleSuitcase : this.suitcases) {
            singleSuitcase.printItems();
        }
    }
}
