
import java.util.ArrayList;

public class Suitcase {

    private int maxWeight;
    private int currentWeight;
    private ArrayList<Item> items = new ArrayList<>();

    public Suitcase(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public void addItem(Item item) {
        if (item.getWeight() + this.currentWeight <= this.maxWeight) {
            this.items.add(item);
            this.currentWeight += item.getWeight();
        }
    }

    public String toString() {

        if (this.items.size() == 0) {
            return "no items (0 kg)";
        }
        if (this.items.size() == 1) {
            return "1 item (" + this.currentWeight + " kg)";
        }
        return this.items.size() + " items (" + this.currentWeight + " kg)";
    }

    public void printItems() {
        for (Item singleItem : this.items) {
            System.out.println(singleItem);
        }
    }

    public int totalWeight() {
        return this.currentWeight;
    }
    
    public Item heaviestItem() {
        if (this.items.isEmpty()) {
            return null;
        }
        
        Item heaviest = this.items.get(0);
        
        for (Item itm: this.items) {
            if (heaviest.getWeight() < itm.getWeight()) {
                heaviest = itm;
            }
        }
        return heaviest;
    }
}
