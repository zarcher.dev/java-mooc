
import java.util.Scanner;

public class UserInterface {

    private Grading register;
    private Scanner scanner;

    public UserInterface(Grading register, Scanner scanner) {
        this.register = register;
        this.scanner = scanner;
    }

    public void start() {
        readGradePoints();
        printAverageOfPoints();
        printAverageOfPassingGradePoints();
        printPassPercentage();
        printGradeDistribution();
    }

    public void readGradePoints() {
        while (true) {
            System.out.println("Enter point totals, -1 stops:");

            String input = scanner.nextLine();

            if (input.equals("-1")) {
                break;
            }

            int inputPoints = Integer.valueOf(input);

            if (inputPoints < 0 || inputPoints > 100) {
                continue;
            }

            this.register.addPoints(inputPoints);
        }
    }

    public void printAverageOfPoints() {
        System.out.println("Point average (all): " + this.register.averageOfGradePoints());
    }

    public void printAverageOfPassingGradePoints() {
        if (this.register.averageOfPassingGradePoints() == -1) {
            System.out.println("Point average (passing): -");
        } else {
            System.out.println("Point average (passing): " + this.register.averageOfPassingGradePoints());
        }
    }

    public void printPassPercentage() {
        if (this.register.passPercentage() == -1) {
            System.out.println("Pass percentage: 0.0");
        } else {
            System.out.println("Pass percentage: " + this.register.passPercentage());
        }
    }

    public void printGradeDistribution() {
        int grade = 5;
        while (grade >= 0) {
            int stars = register.numberOfGrades(grade);
            System.out.print(grade + ": ");
            printStars(stars);
            System.out.println("");

            grade = grade - 1;
        }
    }

    public static void printStars(int stars) {
        while (stars > 0) {
            System.out.print("*");
            stars--;
        }
    }
}
