
import java.util.ArrayList;

public class Grading {

    private ArrayList<Integer> gradePoints;
    private ArrayList<Integer> passingGradePoints;
    private int totalParticipants;
    private int passingParticipants;
    private ArrayList<Integer> gradesList;

    public Grading() {
        this.gradePoints = new ArrayList<>();
        this.passingGradePoints = new ArrayList<>();
        this.totalParticipants = 0;
        this.passingParticipants = 0;
        this.gradesList = new ArrayList<>();
    }

    public void addPoints(int thisGradePoints) {
        this.gradePoints.add(thisGradePoints);
        this.gradesList.add(pointsToGrade(thisGradePoints));
        this.totalParticipants++;

        if (thisGradePoints >= 50) {
            this.addPassingPoints(thisGradePoints);
        }
    }

    public void addPassingPoints(int thisGradePoints) {
        this.passingGradePoints.add(thisGradePoints);
        this.passingParticipants++;
    }

    public double averageOfGradePoints() {
        if (this.gradePoints.isEmpty()) {
            return -1;
        }

        int total = 0;
        for (int thisGradePoint : this.gradePoints) {
            total += thisGradePoint;
        }

        double average = 1.0 * total / gradePoints.size();
        return average;
    }

    public double averageOfPassingGradePoints() {
        if (this.passingGradePoints.isEmpty()) {
            return -1;
        }

        int totalPassing = 0;
        for (int thisPassingGradePoint : this.passingGradePoints) {
            totalPassing += thisPassingGradePoint;
        }

        double averageOfPassingGradePoints = totalPassing * 1.0 / this.passingGradePoints.size();
        return averageOfPassingGradePoints;
    }

    public double passPercentage() {
        if (this.totalParticipants == 0) {
            return -1;
        }

        return 100.0 * this.passingParticipants / this.totalParticipants;
    }

    public int returnParticipants() {
        return this.totalParticipants;
    }
    
    public int numberOfGrades(int grade) {
        int count = 0;
        for (int eachPoint: this.gradesList){
            if (eachPoint == grade) {
                count++;
            }
        }
        
        return count;
    }

    public int pointsToGrade(int points) {
        int grade = 0;
        if (points < 50) {
            grade = 0;
        } else if (points < 60) {
            grade = 1;
        } else if (points < 70) {
            grade = 2;
        } else if (points < 80) {
            grade = 3;
        } else if (points < 90) {
            grade = 4;
        } else {
            grade = 5;
        }

        return grade;
    }
}
