import java.util.Scanner;

public class UserInterface {
    
    private Container firstContainer;
    private Container secondContainer;
    private Scanner scanner;
    
    public UserInterface(Container firstContainer, Container secondContainer, Scanner scanner) {
        this.firstContainer = firstContainer;
        this.secondContainer = secondContainer;
        this.scanner = scanner;
    }
    
    public void start() {
        System.out.println("First: " + firstContainer);
        System.out.println("Second: " + secondContainer);
        
        while (true) {
            String input = scanner.nextLine();
            if (input.equals("quit")){
                break;
            }
            
            String parts[] = input.split(" ");
            String command = parts[0];
            int amount = Integer.valueOf(parts[1]);
            
            if (command.equals("add") && amount > 0) {
                    firstContainer.add(amount);
                    System.out.println("First: " + firstContainer);
                    System.out.println("Second: " + secondContainer);
            }
            
            else if (command.equals("move") && amount >= 0){
                if ((firstContainer.contains() - amount >= 0)) {
                    firstContainer.remove(amount);
                    secondContainer.add(amount);
                } 
                
                else if ((firstContainer.contains() - amount <= 0) && amount >= 0) {
                    secondContainer.add(firstContainer.contains());
                    firstContainer.remove(firstContainer.contains());
                }
                
                System.out.println("First: " + firstContainer);
                System.out.println("Second: " + secondContainer);
            }
            
            else if (command.equals("remove") && amount >= 0) {
                secondContainer.remove(amount);
                System.out.println("First: " + firstContainer);
                System.out.println("Second: " + secondContainer);
            }
        }
    }
}
