import java.util.ArrayList;

public class SimpleCollection {

    private String name;
    private ArrayList<String> elements;

    public SimpleCollection(String name) {
        this.name = name;
        this.elements = new ArrayList<>();
    }

    public void add(String element) {
        this.elements.add(element);
    }

    public ArrayList<String> getElements() {
        return this.elements;
    }

    @Override
    public String toString() {
        String printOutput = "The collection " + this.name;

        if (elements.isEmpty()) {
            return printOutput + " is empty.";
        }

        String eachElement = "";
        int i = 0;
        int s = this.elements.size();
        for (String element : elements) {
            if (i < s - 1) {
                eachElement += element + "\n";
            } else {
                eachElement += element;
            }
        }
        if (elements.size() == 1) {
            return printOutput + " has " + elements.size() + " element:\n" + eachElement;
        }
        return printOutput + " has " + elements.size() + " elements:\n" + eachElement;
    }
}
