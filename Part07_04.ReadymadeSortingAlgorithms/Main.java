import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        int[] integerArray = {3, 1, 5, 99, 3, 12};
        sort(integerArray);
        System.out.println("");

        String[] stringArray = {"Corn", "Nut", "Dolphin", "Pizza pie"};
        sort(stringArray);
        System.out.println("");

        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(3);
        integers.add(1);
        integers.add(9);
        integers.add(99);
        integers.add(7);
        sortIntegers(integers);
        System.out.println("");

        ArrayList<String> strings = new ArrayList<>();
        strings.add("potato");
        strings.add("apple");
        strings.add("pineapple");
        strings.add("zucchini");
        strings.add("sprouts");
        sortStrings(strings);
        System.out.println("");
    }

    public static void sort(int array[]) {
        System.out.println("Before: " + Arrays.toString(array));
        Arrays.sort(array);
        System.out.println("After: " + Arrays.toString(array));
    }

    public static void sort(String[] array) {
        System.out.println("Before: " + Arrays.toString(array));
        Arrays.sort(array);
        System.out.println("After: " + Arrays.toString(array));
    }

    public static void sortIntegers(ArrayList<Integer> integers) {
        System.out.println("Before: " + integers);
        Collections.sort(integers);
        System.out.println("After: " + integers);
    }

    public static void sortStrings(ArrayList<String> strings) {
        System.out.println("Before: " + strings);
        Collections.sort(strings);
        System.out.println("After: " + strings);
    }
}
