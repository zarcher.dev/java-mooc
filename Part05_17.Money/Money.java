public class Money {

    private final int euros;
    private final int cents;

    public Money(int euros, int cents) {

        if (cents > 99) {
            euros = euros + cents / 100;
            cents = cents % 100;
        }

        this.euros = euros;
        this.cents = cents;
    }

    public int euros() {
        return this.euros;
    }

    public int cents() {
        return this.cents;
    }

    public String toString() {
        String zero = "";
        if (this.cents < 10) {
            zero = "0";
        }

        return this.euros + "." + zero + this.cents + "e";
    }

    public Money plus(Money addition) {
        if (this.cents + addition.cents >= 100) {    
            Money newMoney = new Money((this.euros + addition.euros + 1), (this.cents + addition.cents - 100));
            return newMoney;
        }
        
        Money newMoney = new Money ((this.euros + addition.euros), (this.cents + addition.cents));
        return newMoney;
    }

    public boolean lessThan(Money compared) {
        return this.euros + (0.01 * this.cents) <= (compared.euros + (0.01 * compared.cents));
    }

    public Money minus(Money decreaser) {
        if ((this.euros - decreaser.euros >= 1) && (this.cents - decreaser.cents < 0)) {
            Money newMoney = new Money((this.euros - decreaser.euros - 1), (100 + this.cents - decreaser.cents));
            return newMoney;
        }
        
        if ((this.euros - decreaser.euros >= 0) && (this.cents - decreaser.cents >= 0)) {
            Money newMoney = new Money ((this.euros - decreaser.euros), (this.cents - decreaser.cents));
            return newMoney;
        }
        
        Money newMoney = new Money (0, 0);
        return newMoney;
    }

}
