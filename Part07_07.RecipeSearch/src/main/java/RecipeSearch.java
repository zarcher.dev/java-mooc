
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class RecipeSearch {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("File to read: ");
        String inputFile = scanner.nextLine();
        ArrayList<Recipe> recipes = readFromFile(inputFile);

        System.out.println("Commands: ");
        System.out.println("list - lists the recipes");
        System.out.println("stop - stops the program");

        loop:
        while (true) {
            System.out.println("Enter command: ");

            String command = scanner.nextLine();

            switch (command) {
                case "stop":
                    break loop;
                case "list":
                    for (Recipe recipe : recipes) {
                        System.out.println(recipe);
                    }
                    break;
                    
                case "find name":
                    System.out.println("Searched word: ");
                    String searchedWord = scanner.nextLine();
                    System.out.println("Recipes:");
                    searchByName(searchedWord, recipes);
                    break;
                    
                case "find cooking time":
                    System.out.println("Cook time: ");
                    int timeInput = Integer.valueOf(scanner.nextLine());
                    System.out.println("Recipes: ");
                    searchByTime(timeInput, recipes);
                    break;
                    
                case "find ingredient":
                    System.out.println("Ingredient: ");
                    String ingredient = scanner.nextLine();
                    System.out.println("Recipes: ");
                    searchByIngredient(ingredient, recipes);
                    break;
            }
        }
    }

    public static ArrayList<Recipe> readFromFile(String inputFile) {
        ArrayList<Recipe> recipes = new ArrayList<>();

        try (Scanner scanner = new Scanner(Paths.get(inputFile))) {
            while (scanner.hasNextLine()) {
                String name = scanner.nextLine();
                int cookTime = Integer.valueOf(scanner.nextLine());

                Recipe recipe = new Recipe(name, cookTime);

                recipes.add(recipe);

                while (scanner.hasNextLine()) {
                    String ingredient = scanner.nextLine();

                    if (ingredient.isEmpty()) {
                        break;
                    }

                    recipe.addIngredient(ingredient);
                }
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        
        return recipes;
    }

    public static void searchByName(String name, ArrayList<Recipe> recipes) {
        for (Recipe recipe : recipes) {
            if (recipe.getName().contains(name)) {
                System.out.println(recipe);
            }
        }       
    }

    public static void searchByTime(int cookTime, ArrayList<Recipe> recipes) {
        for (Recipe recipe : recipes) {
            if (recipe.getTime() <= cookTime) {
                System.out.println(recipe);
            }
        }
    }

    public static void searchByIngredient(String ingredient, ArrayList<Recipe> recipes) {
        for (Recipe recipe : recipes) {
            if (recipe.getIngredients().contains(ingredient)) {
                System.out.println(recipe);
            }
        }
    }
}
