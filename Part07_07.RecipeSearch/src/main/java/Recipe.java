
import java.util.ArrayList;
import java.util.Scanner;

public class Recipe {
    private String name;
    private int cookTime;
    private ArrayList<String> ingredients;
    
    public Recipe(String name, int cookTime) {
        this.name = name;
        this.cookTime = cookTime;
        this.ingredients = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }
    
    public int getTime() {
        return this.cookTime;
    }
    
    public void addIngredient(String ingredient) {
        this.ingredients.add(ingredient);
    }
    
    public ArrayList<String> getIngredients() {
        return this.ingredients;
    }
       
    @Override
    public String toString() {
        return this.name + ", cooking time: " + cookTime;
    }
}