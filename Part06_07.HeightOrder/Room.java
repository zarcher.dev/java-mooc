
import java.util.ArrayList;

public class Room {

    private ArrayList<Person> peopleInRoom;

    public Room() {
        this.peopleInRoom = new ArrayList<>();
    }

    public void add(Person person) {
        peopleInRoom.add(person);
    }

    public boolean isEmpty() {
        return peopleInRoom.isEmpty();
    }

    public ArrayList<Person> getPersons() {
        return this.peopleInRoom;
    }

    public Person shortest() {
        if (this.peopleInRoom.isEmpty()) {
            return null;
        }

        Person shortest = this.peopleInRoom.get(0);

        for (Person human : this.peopleInRoom) {
            if (shortest.getHeight() > human.getHeight()) {
                shortest = human;
            }
        }
        
        return shortest;
    }
    
    public Person take(){
        Person shortest = this.shortest();
        
        if (this.peopleInRoom.isEmpty()){
            return null;
        }
        
        peopleInRoom.remove(this.shortest());
        return shortest;
        
    }
}
